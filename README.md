An E-commerce website with Django

Steps to run:

1. Create a virtual environment
2. Activate the virtual environment
3. Install the requirements by pip install -r requirements.txt
4. Make migrations : python manage.py makemigrations
5. Migrate : python manage.py migrate
6. Create a superuser : python manage.py createsuperuser
7. Login to localhost:8000/admin
8. Add the Product in items
9. Add the stock quantity, Sizes.
10. Goto localhost:8000 for ecommerce site